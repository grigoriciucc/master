#include <iostream> // cout, endl

#include <mpi.h>

using namespace ::std;

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
    cout << "Hello" << endl;
    MPI_Finalize();

    return 0;
}