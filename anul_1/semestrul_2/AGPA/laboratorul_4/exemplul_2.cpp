#include <cstring>  // strcpy
#include <iostream> // cout, endl

#include <mpi.h>

using namespace ::std;

int main(int argc, char** argv)
{
    char message[40];
    int my_rank;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    if(my_rank == 0)
    {
        strcpy(message, "Hello, there!");
        MPI_Send(message, strlen(message) + 1, MPI_CHAR, 1, 99, MPI_COMM_WORLD);
        cout << "[Master] Sent: " << message << endl;
    }
    else if(my_rank == 1)
    {
        MPI_Recv(message, 20, MPI_CHAR, 0, 99, MPI_COMM_WORLD, &status);
        cout << "[Slave] Received: " << message << endl;
    }
    else
    {
        cout << "[Error] You should only launch 2 processes (my_rank = " << my_rank << ")." << endl;
    }

    MPI_Finalize();

    return 0;
}