#include <algorithm> // find_if
#include <cstring>   // strcpy
#include <iostream>  // cout, endl
#include <string>    // string
#include <string_view> // string_view

#include <mpi.h>

using namespace ::std;

int count_vowels(const string_view text)
{
    int number_of_vowels{0};

    for(const auto& character : text)
    {
        constexpr char vowels[] = "aeiouAEIOU";
        if(find(begin(vowels), end(vowels), character) != end(vowels))
        {
            number_of_vowels++;
        }
    }

    return number_of_vowels;
}

int main(int argc, char** argv)
{
    string text;
    char message[50];
    int my_rank;
    MPI_Status status;
    int world_size;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    if(my_rank == 0)
    {
        cout << "[Master] Give me a text: ";
        getline(cin, text);
        cout << "[Master] I've readed from keyboard: \"" << text << "\"." << endl;

        int start_index{0};
        const int number_of_characters_per_slave = text.size() / world_size;

        int number_of_vowels_in_text{count_vowels(string_view(text.data(), number_of_characters_per_slave))};
        cout << string_view(text.data(), number_of_characters_per_slave) << endl;
        cout << "[Master] I've counted " << number_of_vowels_in_text << " vowels." << endl;

        for(int slave_id = 1; slave_id <= world_size - 1; slave_id++)
        {
            start_index += number_of_characters_per_slave;
            MPI_Send(start_index + text.data(), number_of_characters_per_slave, MPI_CHAR, slave_id, 99, MPI_COMM_WORLD);
            cout << "[Master] Sent \"" << string_view(text.data() + start_index, number_of_characters_per_slave) << "\" to slave_id = " << slave_id << "." << endl;
            int number_of_vowels_from_slave{0};
            MPI_Recv(&number_of_vowels_from_slave, 8, MPI_INTEGER, slave_id, 99, MPI_COMM_WORLD, &status);
            number_of_vowels_in_text += number_of_vowels_from_slave;
        } 

        cout << "[Master] The text has " << number_of_vowels_in_text << " vowels." << endl;
    }
    else // my_rank != 0
    {
        MPI_Recv(message, 20, MPI_CHAR, 0, 99, MPI_COMM_WORLD, &status);
        text = string(message, strlen(message) - 2);
        cout << "[Slave][" << my_rank << "] Received \"" << text << "\" from master." << endl;
        int number_of_vowels{count_vowels(string_view(text))};
        cout << "[Slave][" << my_rank << "] I've counted " << number_of_vowels << " vowels." << endl;
        MPI_Send(&number_of_vowels, 8, MPI_INTEGER, 0, 99, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    return 0;
}