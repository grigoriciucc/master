#include <cstring>  // strcpy
#include <iostream> // cout, endl

#include <mpi.h>

using namespace ::std;

int main(int argc, char** argv)
{
    char message[40];
    int my_rank;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    if(my_rank % 2 == 0)
    {
        strcpy(message, "Hello, there!");
        MPI_Send(message, strlen(message) + 1, MPI_CHAR, my_rank + 1, 99, MPI_COMM_WORLD);
        cout << "[Master][" << my_rank << "] Sent \"" << message << "\" to rank = " << my_rank + 1 << "." << endl;
    }
    else // my_rank % 2 == 1
    {
        MPI_Recv(message, 20, MPI_CHAR, my_rank - 1, 99, MPI_COMM_WORLD, &status);
        cout << "[Slave][" << my_rank << "] Received \"" << message << "\" from rank = " << my_rank - 1 << "." << endl;
    }


    MPI_Finalize();

    return 0;
}