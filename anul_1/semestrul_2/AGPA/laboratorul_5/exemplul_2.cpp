#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  int myrank, size;
  int i, j, mym = 10, myn = 10;
  int a[10][10], b[5][5], c[5][5];
  MPI_Datatype subrow, submatrix;
  MPI_Aint sizeofint; /* Initializarea comunicatiei MPI */
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &size); 
  
  /* Initializarea matricei initiale */
  for (i = 0; i < mym; ++i)
    for (j = 0; j < myn; ++j)
      a[i][j] = i * myn + j + 1; /* Afiseaza matricea initiala */
  if (myrank == 0) {
    for (i = 0; i < mym; ++i) {
      for (j = 0; j < myn; ++j)
        printf("%d ", a[i][j]);
      printf("\n");
    }
  } 
  
  /*       
   * Afla dimensiunea  tipului de date MPI_INT 
   */
  MPI_Type_extent(MPI_INT,
                  &sizeofint); /* Creaza tipul de date pentru „sub-randuri” */
  MPI_Type_vector(5, 1, 2, MPI_INT,
                  &subrow); /* Validarea noului tip de creat */
  MPI_Type_commit(
      &subrow); /* Creaza tipului de date „”sub-matrice”       *
                   „MPI_Type_hvector” este identic cu „MPI_Type_vector” cu
                   * exceptia ca acest pas este exprimat in Bytes */
  MPI_Type_hvector(5, 1, 20 * sizeofint, subrow,
                   &submatrix); /* Validarea noului tip de date  */
  MPI_Type_commit(&submatrix);  /* Trimite noul tip de date si-l afiseaza */
  MPI_Sendrecv(a, 1, submatrix, myrank, 0, c, 5 * 5, MPI_INT, myrank, 0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE); /* Afiseaza „sub-matricea’ */
  if (myrank == 0) {
    for (i = 0; i < 5; ++i) {
      for (j = 0; j < 5; ++j)
        printf("%d ", c[i][j]);
      printf("\n");
    }
  } /* Finalizarea comunicatiei MPI */
  MPI_Finalize();
  return 0;
}