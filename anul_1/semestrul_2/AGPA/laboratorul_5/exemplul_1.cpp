#include <math.h>
#include <mpi.h>
#include <stdio.h>

/*   * Declarare functie - calcul PI */
double f(double);

/*   * Definitie         - calcul PI */
double f(double a) {
  // valoarea returnata
  return (4.0 / (1.0 + a * a));
}

/*   * Functia main( ... ) */
int main(int argc, char *argv[]) {
  int done = 0, n, myid, numprocs, i;
  double PI25DT = 3.141592653589793238462643;
  double mypi, pi, h, sum, x;
  double startwtime = 0.0, endwtime;
  int namelen;
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  /*  * Citeste numele NODULUI */
  MPI_Get_processor_name(processor_name, &namelen);

  /*  * Procesul curent */
  printf("Procesul [%d] pe <%s>\n", myid, processor_name);
  n = 0;

  while (!done) {
    if (myid == 0) // Daca este nodul [MASTER]
    {
      /*
      printf("Introdu numarul de  intervale [ 0 - iesire ] ");
      scanf("%d",&n ); */
      if (n == 0)
        n = 100;
      else
        n = 0;
      /*
       * Timpul INITIAL */
      startwtime = MPI_Wtime();
    }

    /*
     * Trimite catre toate nodurile */
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    if (n == 0)
      done = 1;
    else {
      h = 1.0 / (double)n;
      sum = 0.0;

      for (i = myid + 1; i <= n; i += numprocs) {
        x = h * ((double)i - 0.5);
        sum += f(x);
      } /* Obtinem o valoare PI partiala */

      mypi = h * sum;

      /*
       * Reduce(cumuleaza) valorile partiale ( de la procese)
       * la valoarea cumulativa - MPI_SUM - pentru PI
       */
      MPI_Reduce(&mypi, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

      /*
       * Daca este procesul parinte */

      if (myid == 0) {
        printf("\t- Procesul [%d] pe <%s>\n", myid, processor_name);
        printf("\t- PI este aproximativ: %.16f;"
               "\n\t- Eroarea obtinuta: %.16f\n",
               pi, fabs(pi - PI25DT));
        endwtime = MPI_Wtime();
        printf("\t- Diferenta de timp obtinuta: %.16f\n",
               endwtime - startwtime);
      }
    }
  }

  MPI_Finalize();
  return 0;
}