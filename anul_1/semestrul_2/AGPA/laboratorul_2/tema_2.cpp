#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <charconv> // from_chars
#include <fstream> // fstream
#include <iostream> // cout, endl
#include <string> // string

using namespace ::std;

void signal_handler(int signal)
{
    cout << "Child got a signal from parent, so let's make that sum." << endl;
}

int main( void )
{       
    pid_t pid = fork();

    if(pid == -1)
    {
        cout << "Error during child process creation." << endl;
        return 1;
    }
    
    if(pid == 0)
    {
        signal(SIGUSR1, signal_handler);
        pause();

        fstream file("values.txt", file.in);
        if(!file.is_open())
        {
            cout << "Something went wrong and we couldn't open the file to store those values." << endl;
            return 1;
        }

        unsigned int sum{0};
        string line;

        while(getline(file, line))
        {
            int converted_value{0};

            if(from_chars(line.data(), line.data() + line.size(), converted_value).ec == errc::invalid_argument)
            {
                cout << "Error during the conversion of the line to an integer." << endl;
                return 1;
            }

            sum += converted_value;
        }

        cout << "The sum is " << sum << "." << endl;

        return 0;
    }

    cout << "Parent process: " << endl;

    fstream file("values.txt", file.out);
    if(!file.is_open())
    {
        cout << "Something went wrong and we couldn't open the file to store those values." << endl;
        return 1;
    }

    unsigned int first_value{0};
    unsigned int second_value{0};

    cout << "Give me the first value: " << endl;
    cin >> first_value;
    cout << "Give me the second value: " << endl;
    cin >> second_value;

    cout << "The provided values are the following ones: " << first_value << " and " << second_value << "." << endl;

    // The file was successfully opened or created (if it couldn't be found).
    file << first_value << endl << second_value << endl;
    file.close();
    kill(pid, SIGUSR1);

    cout << "Parent waits for child to finish." << endl;
    int pstatus{0};
    waitpid(pid, &pstatus, 0);

    cout << "Both processes were completed." << endl;

    return 0;
}