#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int citesteSir( int s[] );

int main( void )
{       
    char comm[80], *fisier = "sortat.txt";
    int sir[100], N;

    system( "clear" );
    do
    {       
        printf("SERVER$ ");
        scanf( "%s", comm );

        // Daca este comanda de Sortare
        if( strcmp( comm, ":s") == 0 )
        {
            N = citesteSir( sir );
            printf( "Sirul preluat are %d elemente\n", N );
            //marcheaza faptul ca incepe sortarea
            //      - blochez semaforul

            //Fork
            pid_t pid = fork();
            
            if(pid == -1)
            {
                printf("Eroare la crearea unui nou proces.\n");
                exit(1);
            }
            else if(pid == 0)
            {
                printf("Procesul fiu:\n");
            }
            else
            {
                printf("Procesul parinte:\n");
            }


            //daca sunt parinte astept finalizarea sortarii, prin
            //      - testarea semaforului, daca s-a deblocat

            //daca sunt fiu ...
            //      - sortez
            //      - copii sirul sortat in fisierul sortat.txt
            //      - deblochez semaforul
            //      - exit();

            //Afisez - deoarece doar parintele ajunge aici

        }
        else if( strcmp( comm, ":q") == 0 )
        {       
        printf( "FINISH\n" );
            return 0;
        }
        else if( strcmp( comm, ":c") == 0 )
            system( "clear" );
        else if( strcmp( comm, ":h") == 0 )
            printf( "\tHELP\n\t:q - parasire program"
                "\n\t:h - Help"
                "\n\t:c - stergere ecran"
                "\n\t:s - preluare sir + sortare\n" );
        else    
        printf( "Comanda necunoscuta!\nFolositi ':h'"
            " pentru Help.\n" );
    }while( !0 );

    return 0;
}

int citesteSir( int s[] )
{
    int n = 0, i;
    printf("\tIntrodu nr. de elemente: " );
    scanf( "%d", &n );

    for( i = 0; i < n; i++ )
    {
        printf( "S[ %d ] = ", i );
        scanf ( "%d", &s[ i ] );
    }
    return n;
}